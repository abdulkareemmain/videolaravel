
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Video Slide Show Maker</title>
    <base href="/">

    <link rel="apple-touch-icon" sizes="76x76" href="/apple-touch-icon.png?v=1.0.3">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png?v=1.0.3">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png?v=1.0.3">
    <link rel="manifest" href="/site.webmanifest?v=1.0.3">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">
                <link href="/bundle/styles.css?v=1.0.3" rel="stylesheet">

</head>
<body class="app-bg">

    <div class="app-container py-5">
        <div class="container pt-md-5">

            <div class="row">
                <div class="col-lg-8 offset-lg-2">
                    <div class="card card-body mt-5 bg-white-transp rounded">

                        <h1 class="text-center">Video Slide Show Maker</h1>

                        <app-root><div id="page-preloader" class="loading"></div></app-root>

                    </div>
                </div>
            </div>

        </div>
    </div>

    <script>
        var appSettings = {
            appName: 'Video Slide Show Maker',
            webApiUrl: '',
            environment: 'prod',
            locale: 'en',
            displayLanguageSwitch: true,
            displayHomepageVideo: true,
            displayHomepageFeatures: true,
            languages: [{"code":"en","name":"English","icon":"flag-gb"},{"code":"ru","name":"\u0420\u0443\u0441\u0441\u043a\u0438\u0439","icon":"flag-ru"}]
        };
    </script>
    <script type="text/javascript" src="{{asset('/assets/i18n/en.js?v=1.0.3')}}"></script>
            <script type="text/javascript" src="{{asset('/bundle/runtime.js?v=1.0.3')}}"></script>
        <script type="text/javascript" src="{{asset('/bundle/polyfills.js?v=1.0.3')}}"></script>
        <script type="text/javascript" src="{{asset('/bundle/scripts.js?v=1.0.3')}}"></script>
        <script type="text/javascript" src="{{asset('/bundle/main.js?v=1.0.3')}}"></script>

    <div style="text-align: center; margin: 0 0 30px 0;">
        <!-- Your banner ad code is here -->
        <!--a href="#" target="_blank">
            <img src="/assets/img/468x60.gif" width="468" height="60" alt="">
        </a-->
    </div>

</body>
</html>
