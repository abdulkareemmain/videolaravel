<html>
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <title>Video Slide Show Maker</title>
      <base href="/">
      <link rel="apple-touch-icon" sizes="76x76" href="/apple-touch-icon.png?v=1.0.3">
      <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png?v=1.0.3">
      <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png?v=1.0.3">
      <link rel="manifest" href="/site.webmanifest?v=1.0.3">
      <meta name="msapplication-TileColor" content="#da532c">
      <meta name="theme-color" content="#ffffff">
      <link href="/bundle/styles.css?v=1.0.3" rel="stylesheet">
      <style></style>
   </head>
   <body class="app-bg">
      <div class="app-container py-5">
         <div class="container pt-md-5">
            <div class="row">
               <div class="col-lg-8 offset-lg-2">
                  <div class="card card-body mt-5 bg-white-transp rounded">
                     <h1 class="text-center">Video Slide Show Maker</h1>
                     <app-root _nghost-kox-c0="" ng-version="7.2.16">
                        <!---->
                        <div _ngcontent-kox-c0="" class="position-fixed" style="top: 15px; right: 25px;">
                           <!----><!----><!----><!----><!----><!---->
                           <div _ngcontent-kox-c0="" class="d-inline-block ml-3"><a _ngcontent-kox-c0="" class="text-white" href="#"><i _ngcontent-kox-c0="" class="flag flag-ru"></i> Русский </a></div>
                           <!---->
                        </div>
                        <router-outlet _ngcontent-kox-c0=""></router-outlet>
                        <app-project _nghost-kox-c1="">
                           <div _ngcontent-kox-c1="" class="position-relative">
                              <div _ngcontent-kox-c1="" class="position-absolute" style="left: 0; top: -80px;"><a _ngcontent-kox-c1="" class="btn btn-secondary btn-icon btn-pill" routerlink="/home" title="Home page" href="#/home"><i _ngcontent-kox-c1="" class="fe fe-home"></i></a></div>
                           </div>
                           <div _ngcontent-kox-c1="" class="dimmer">
                              <div _ngcontent-kox-c1="" class="loader"></div>
                              <div _ngcontent-kox-c1="" class="min-height150 dimmer-content">
                                 <div _ngcontent-kox-c1="" class="my-5">
                                    <!----><!----><!---->
                                    <div _ngcontent-kox-c1="" dragula="mediaItems">
                                       <!---->
                                       <div _ngcontent-kox-c1="" class="card rounded">
                                          <div _ngcontent-kox-c1="" class="card-body py-2 px-3">
                                             <div _ngcontent-kox-c1="" class="row">
                                                <div _ngcontent-kox-c1="" class="col-6">
                                                   <div _ngcontent-kox-c1="" class="row">
                                                      <div _ngcontent-kox-c1="" class="col-2 text-muted py-5 drag-handle">
                                                         <div _ngcontent-kox-c1="" class="font-middle"><i _ngcontent-kox-c1="" class="d-block fe fe-more-horizontal"></i><i _ngcontent-kox-c1="" class="d-block fe fe-more-horizontal" style="margin-top: -1.4rem;"></i><i _ngcontent-kox-c1="" class="d-block fe fe-more-horizontal" style="margin-top: -1.4rem;"></i></div>
                                                      </div>
                                                      <div _ngcontent-kox-c1="" class="col-10">
                                                         <div _ngcontent-kox-c1="" class="position-relative text-center max-width-100 mx-auto show-on-hover-parent">
                                                         <img _ngcontent-kox-c1="" alt="" class="rounded-circle max-width-100" src="{{asset('userfiles/projects/k9de497rWvPV5f8f4343b2c2a/119d45a9d4ee77cb4f592cf3e2945b40.jpg')}}">
                                                            <div _ngcontent-kox-c1="" class="position-absolute w-100 h-100 bg-white-transp show-on-hover" style="left: 0; top: 0;"><button _ngcontent-kox-c1="" class="btn btn-secondary btn-icon btn-pill btn-icon-center" type="button" title="Edit"><i _ngcontent-kox-c1="" class="fe fe-edit"></i></button></div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div _ngcontent-kox-c1="" class="col-6">
                                                   <div _ngcontent-kox-c1="" class="row">
                                                      <div _ngcontent-kox-c1="" class="col-8 pt-2"><label _ngcontent-kox-c1="" for="fieldTextContent0"> Overlay text: </label><input _ngcontent-kox-c1="" class="form-control ng-untouched ng-pristine ng-valid" type="text" id="fieldTextContent0"></div>
                                                      <div _ngcontent-kox-c1="" class="col-4 text-right pt-5"><label _ngcontent-kox-c1="" class="custom-switch mt-4 pr-2 cursor-pointer"><input _ngcontent-kox-c1="" class="custom-switch-input ng-untouched ng-pristine ng-valid" type="checkbox" value="1"><span _ngcontent-kox-c1="" class="custom-switch-indicator"></span></label></div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div _ngcontent-kox-c1="" class="card rounded">
                                          <div _ngcontent-kox-c1="" class="card-body py-2 px-3">
                                             <div _ngcontent-kox-c1="" class="row">
                                                <div _ngcontent-kox-c1="" class="col-6">
                                                   <div _ngcontent-kox-c1="" class="row">
                                                      <div _ngcontent-kox-c1="" class="col-2 text-muted py-5 drag-handle">
                                                         <div _ngcontent-kox-c1="" class="font-middle"><i _ngcontent-kox-c1="" class="d-block fe fe-more-horizontal"></i><i _ngcontent-kox-c1="" class="d-block fe fe-more-horizontal" style="margin-top: -1.4rem;"></i><i _ngcontent-kox-c1="" class="d-block fe fe-more-horizontal" style="margin-top: -1.4rem;"></i></div>
                                                      </div>
                                                      <div _ngcontent-kox-c1="" class="col-10">
                                                         <div _ngcontent-kox-c1="" class="position-relative text-center max-width-100 mx-auto show-on-hover-parent">
                                                            <img _ngcontent-kox-c1="" alt="" class="rounded-circle max-width-100" src="media/cache/resolve/squared_thumbnail/userfiles/projects/L3EpKTxgH9n65f8f68ff14ba7/3cf053b93f47ee368518a9c47fa3816e.jpg">
                                                            <div _ngcontent-kox-c1="" class="position-absolute w-100 h-100 bg-white-transp show-on-hover" style="left: 0; top: 0;"><button _ngcontent-kox-c1="" class="btn btn-secondary btn-icon btn-pill btn-icon-center" type="button" title="Edit"><i _ngcontent-kox-c1="" class="fe fe-edit"></i></button></div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div _ngcontent-kox-c1="" class="col-6">
                                                   <div _ngcontent-kox-c1="" class="row">
                                                      <div _ngcontent-kox-c1="" class="col-8 pt-2"><label _ngcontent-kox-c1="" for="fieldTextContent1"> Overlay text: </label><input _ngcontent-kox-c1="" class="form-control ng-untouched ng-pristine ng-valid" type="text" id="fieldTextContent1"></div>
                                                      <div _ngcontent-kox-c1="" class="col-4 text-right pt-5"><label _ngcontent-kox-c1="" class="custom-switch mt-4 pr-2 cursor-pointer"><input _ngcontent-kox-c1="" class="custom-switch-input ng-untouched ng-pristine ng-valid" type="checkbox" value="1"><span _ngcontent-kox-c1="" class="custom-switch-indicator"></span></label></div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div _ngcontent-kox-c1="" class="mb-3">
                                       <div _ngcontent-kox-c1="" class="text-center">
                                          <input _ngcontent-kox-c1="" accept="audio/*" class="d-none" type="file"><button _ngcontent-kox-c1="" class="btn btn-secondary d-inline-block vertical-top mb-2 mr-2" type="button"><i _ngcontent-kox-c1="" class="fe fe-upload"></i> Select audio file </button>
                                          <select _ngcontent-kox-c1="" class="form-control d-inline-block w-auto vertical-top mb-2 mr-2 ng-untouched ng-pristine ng-valid">
                                             <!---->
                                             <option _ngcontent-kox-c1="" value="Dreams.mp3">Dreams.mp3</option>
                                             <option _ngcontent-kox-c1="" value="Energy.mp3">Energy.mp3</option>
                                             <option _ngcontent-kox-c1="" value="Hey.mp3">Hey.mp3</option>
                                             <option _ngcontent-kox-c1="" value="Summer.mp3">Summer.mp3</option>
                                          </select>
                                          <button _ngcontent-kox-c1="" class="btn btn-success d-inline-block vertical-top mb-2" type="button" title="Play/Pause">
                                             <!----><i _ngcontent-kox-c1="" class="fe fe-play"></i><!---->
                                          </button>
                                       </div>
                                    </div>
                                    <div _ngcontent-kox-c1="" class="text-center">
                                       <div _ngcontent-kox-c1="" class="btn-group btn-group-toggle" data-toggle="buttons"><label _ngcontent-kox-c1="" class="btn btn-secondary active"><input _ngcontent-kox-c1="" autocomplete="off" name="fieldOptionssize" type="radio" value="hd" class="ng-untouched ng-pristine ng-valid"> HD </label><label _ngcontent-kox-c1="" class="btn btn-secondary"><input _ngcontent-kox-c1="" autocomplete="off" name="fieldOptionssize" type="radio" value="full_hd" class="ng-untouched ng-pristine ng-valid"> Full HD </label></div>
                                       <button _ngcontent-kox-c1="" class="btn btn-pill btn-primary btn-lg ml-4" type="button"> Create Video </button>
                                    </div>
                                    <!----><!----><!---->
                                 </div>
                              </div>
                           </div>
                        </app-project>
                     </app-root>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <script>
         var appSettings = {
             appName: 'Video Slide Show Maker',
             webApiUrl: '',
             environment: 'prod',
             locale: 'en',
             displayLanguageSwitch: true,
             displayHomepageVideo: true,
             displayHomepageFeatures: true,
             languages: [{"code":"en","name":"English","icon":"flag-gb"},{"code":"ru","name":"\u0420\u0443\u0441\u0441\u043a\u0438\u0439","icon":"flag-ru"}]
         };
      </script>
      <script type="text/javascript" src="{{asset('/assets/i18n/en.js?v=1.0.3')}}"></script>
            <script type="text/javascript" src="{{asset('/bundle/runtime.js?v=1.0.3')}}"></script>
        <script type="text/javascript" src="{{asset('/bundle/polyfills.js?v=1.0.3')}}"></script>
        <script type="text/javascript" src="{{asset('/bundle/scripts.js?v=1.0.3')}}"></script>
        <script type="text/javascript" src="{{asset('/bundle/main.js?v=1.0.3')}}"></script>

      <canvas width="1903" height="351" style="position: fixed; top: 0px; left: 0px; z-index: -1;"></canvas>
      <div style="text-align: center; margin: 0 0 30px 0;">
         <!-- Your banner ad code is here -->
         <!--a href="#" target="_blank">
            <img src="/assets/img/468x60.gif" width="468" height="60" alt="">
            </a-->
      </div>
   </body>
</html>
