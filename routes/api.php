<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::get('/project_progress/{id}','CheckController@project_progress');
Route::post('/media','CheckController@index');
Route::get('/project/{id}','CheckController@edit');
Route::post('/project/{id}','CheckController@store');
Route::get('/library_music','CheckController@libraryMusicAction');




