<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Translation\TranslatorInterface;

class BaseController extends Controller
{

    /**
     * @param $userId
     * @return string
     */
    public function getTmpUserDirPath($userId)
    {
        return $this->getParameter('app.temp_dir_path') . "/{$userId}";
    }

    /**
     * @param $queryString
     * @return mixed
     */
    public function getQueryOptions($queryString)
    {
        parse_str($queryString, $options);
        return $options;
    }

    /**
     * @param $message
     * @param int $status
     * @return JsonResponse
     */
    public function setError($message, $status = Response::HTTP_UNPROCESSABLE_ENTITY)
    {
        /** @var TranslatorInterface $translator */
        $translator = $this->get('translator');
        $response = new JsonResponse(["error" => $translator->trans($message)]);
        $response = $response->setStatusCode($status);
        return $response;
    }
}


