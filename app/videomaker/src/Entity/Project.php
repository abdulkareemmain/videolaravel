<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProjectRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Project
{
    const STATUS_NEW = 'new';
    const STATUS_PENDING = 'pending';
    const STATUS_PROCESSING = 'processing';
    const STATUS_COMPLETED = 'completed';
    const STATUS_CANCELED = 'canceled';
    const STATUS_ERROR = 'error';

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $uniqueId;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updatedAt;

    /**
     * @ORM\Column(type="string", length=128)
     */
    private $status;

    /**
     * @ORM\Column(type="string", length=512, nullable=true)
     */
    private $options;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isPublic;

    /**
     * @var Media
     * @ORM\OneToOne(targetEntity="Media", cascade={"persist", "remove"})
     */
    private $movie;

    /**
     * @ORM\PrePersist
     */
    public function prePersist()
    {
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
    }

    /**
     * @ORM\PreUpdate
     */
    public function preUpdate()
    {
        $this->updatedAt = new \DateTime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUniqueId(): ?string
    {
        return $this->uniqueId;
    }

    public function setUniqueId(string $uniqueId): self
    {
        $this->uniqueId = $uniqueId;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getIsPublic(): ?bool
    {
        return $this->isPublic;
    }

    public function setIsPublic(bool $isPublic): self
    {
        $this->isPublic = $isPublic;

        return $this;
    }

    public function getOptions(): ?array
    {
        return $this->options ? json_decode($this->options, true) : [];
    }

    /**
     * @param array $options
     * @return Project
     */
    public function setOptions(array $options): self
    {
        $this->options = json_encode($options);
        return $this;
    }

    /**
     * @param $optionName
     * @return mixed|string
     */
    public function getOptionValue($optionName)
    {
        $options = $this->getOptions();
        return $options[$optionName] ?? '';
    }

    /**
     * @param $optionName
     * @param $value
     * @return $this
     */
    public function setOptionValue($optionName, $value)
    {
        $options = $this->getOptions();
        if (!is_array($options)) {
            $options = [];
        }
        $options[$optionName] = $value;
        $this->setOptions($options);
        return $this;
    }

    public function getMovie(): ?Media
    {
        return $this->movie;
    }

    public function setMovie(?Media $movie): self
    {
        $this->movie = $movie;
        return $this;
    }
}
