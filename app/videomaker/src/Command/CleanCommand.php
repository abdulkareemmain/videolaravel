<?php

namespace App\Command;

use App\Entity\Media;
use App\Entity\Project;
use App\Repository\MediaRepository;
use App\Repository\ProjectRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Doctrine\Form\ChoiceList\EntityLoaderInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class CleanCommand extends ContainerAwareCommand
{
    protected static $defaultName = 'app:clean';

    protected function configure()
    {
        $this
            ->setDescription('Delete old projects');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        /** @var EntityManagerInterface $em */
        $em = $this->getContainer()->get('doctrine')->getManager();

        $uploadDirPath = $this->getUploadDirPath();

        /** @var ProjectRepository $projectRepository */
        $projectRepository = $em->getRepository(Project::class);
        /** @var MediaRepository $mediaRepository */
        $mediaRepository = $em->getRepository(Media::class);
        $count = 0;

        $oldProjects = $projectRepository->findOld($this->getContainer()->getParameter('app.projects_store_hours'));

        /** @var Project $project */
        foreach ($oldProjects as $project) {
            $projectDirPath = $uploadDirPath . DIRECTORY_SEPARATOR . $project->getUniqueId();

            $movieMedia = $project->getMovie();
            $mediaList = $mediaRepository->findProjectMedia($project->getId());

            foreach ($mediaList as $media) {
                if ($movieMedia && $movieMedia->getId() === $media->getId()) {
                    $project->setMovie(null);
                }
                $em->remove($media);
            }
            $em->remove($project);
            $em->flush();

            $count++;
        }

        $io->success("Deleted projects: {$count}");
    }

    /**
     * @return bool|string
     */
    public function getRootPath()
    {
        $rootPath = realpath($this->getContainer()->getParameter('kernel.root_dir') . '/../');
        return $rootPath;
    }

    /**
     * @return string
     */
    public function getUploadDirPath()
    {
        return realpath($this->getContainer()->getParameter('app.project_dir_path'));
    }
}
