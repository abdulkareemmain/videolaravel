<?php

namespace App\EventSubscriber;

use App\Entity\Media;
use App\Entity\Project;
use Doctrine\Common\EventSubscriber;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class DoctrineEventSubscriber implements EventSubscriber
{

    /** @var ContainerInterface */
    private $container;

    /**
     * DoctrineEventSubscriber constructor.
     * @param $container
     */
    public function __construct($container) {
        $this->container = $container;
    }

    public function getSubscribedEvents()
    {
        return array(
            'postPersis',
            'postRemove',
            'postUpdate',
        );
    }

    public function postUpdate(LifecycleEventArgs $args)
    {
        $entity = $args->getObject();
    }

    public function postPersist(LifecycleEventArgs $args)
    {
        $entity = $args->getObject();
    }

    public function postRemove(LifecycleEventArgs $args)
    {
        $entity = $args->getObject();
        /** @var EntityManagerInterface $em */
        $em = $this->container->get('doctrine')->getManager();
        $uploadDirPath = $this->getUploadDirPath();

        if ($entity instanceof Media) {

            /** @var Project $project */
            $project = $em->getRepository(Project::class)->find($entity->getProjectId());
            if ($project) {
                $projectDirPath = $uploadDirPath . DIRECTORY_SEPARATOR . $project->getUniqueId();
                $entity->setFileTargetDir($projectDirPath);
                $filePath = $entity->getFilePath();
                if (file_exists($filePath)) {
                    unlink($filePath);
                }
            }
        }
        else if ($entity instanceof Project) {

            $projectDirPath = $uploadDirPath . DIRECTORY_SEPARATOR . $entity->getUniqueId();
            $this->deleteEmptyDir($projectDirPath);
        }

    }

    /**
     * @param $projectDirPath
     */
    public function deleteEmptyDir($projectDirPath)
    {
        if (!is_dir($projectDirPath)) {
            return;
        }
        $files = scandir($projectDirPath);
        $files = array_filter($files, function($fileName) {
            return !in_array($fileName, ['.', '..']);
        });
        if (count($files) === 0) {
            rmdir($projectDirPath);
        }
    }

    /**
     * @return bool|string
     */
    public function getRootPath()
    {
        $rootPath = realpath($this->container->getParameter('kernel.root_dir') . '/../');
        return $rootPath;
    }

    /**
     * @return string
     */
    public function getUploadDirPath()
    {
        return realpath($this->container->getParameter('app.project_dir_path'));
    }
}
