<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* errors/404.html.twig */
class __TwigTemplate_b688b0c41b886c4355716aa9383cdb5d99bc4fe14adf66651e8c75b82aaab4d1 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'stylesheets' => [$this, 'block_stylesheets'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("base.html.twig", "errors/404.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_stylesheets($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 4
        echo "    ";
        if ((twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "environment", [], "any", false, false, false, 4) == "prod")) {
            // line 5
            echo "        <link href=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("bundle/styles.css"), "html", null, true);
            echo "\" rel=\"stylesheet\">
    ";
        }
    }

    // line 9
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 10
        echo "    <main role=\"main\" class=\"container\">
        
        <div class=\"my-5\" style=\"min-height: 400px;\">
            <div class=\"card bg-white-transp rounded\">
                <div class=\"card-body\">
                    <div class=\"min-height400\">
                        <h1>404 - ";
        // line 16
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Page not found"), "html", null, true);
        echo "</h1>

                        ";
        // line 18
        if ((array_key_exists("message", $context) &&  !twig_test_empty(($context["message"] ?? null)))) {
            // line 19
            echo "                            <div class=\"alert alert-danger\">";
            echo twig_escape_filter($this->env, ($context["message"] ?? null), "html", null, true);
            echo "</div>
                        ";
        }
        // line 21
        echo "                        
                        <div>
                            <a href=\"";
        // line 23
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("homepage");
        echo "\">";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Home page"), "html", null, true);
        echo "</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
    </main>
";
    }

    public function getTemplateName()
    {
        return "errors/404.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  91 => 23,  87 => 21,  81 => 19,  79 => 18,  74 => 16,  66 => 10,  62 => 9,  54 => 5,  51 => 4,  47 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "errors/404.html.twig", "C:\\wamp64\\www\\videomaker\\templates\\errors\\404.html.twig");
    }
}
