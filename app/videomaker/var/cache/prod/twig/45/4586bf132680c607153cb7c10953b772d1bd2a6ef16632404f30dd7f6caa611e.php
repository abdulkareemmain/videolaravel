<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* nav/pagination_simple.html.twig */
class __TwigTemplate_786627b8bbccb6e52fd1422308471318d4c9201a4fc7c8d0f6f6d9513f66408c extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        if ((twig_get_attribute($this->env, $this->source, ($context["pagesOptions"] ?? null), "total", [], "any", false, false, false, 1) > 1)) {
            // line 2
            echo "    <nav>
        <ul class=\"pagination pagination-rounded\">
            ";
            // line 4
            if ((twig_get_attribute($this->env, $this->source, ($context["pagesOptions"] ?? null), "current", [], "any", false, false, false, 4) != twig_get_attribute($this->env, $this->source, ($context["pagesOptions"] ?? null), "prev", [], "any", false, false, false, 4))) {
                // line 5
                echo "                <li class=\"page-item\">
                    <a class=\"page-link\" href=\"";
                // line 6
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, false, false, 6), "attributes", [], "any", false, false, false, 6), "get", [0 => "_route"], "method", false, false, false, 6), ["page" => twig_get_attribute($this->env, $this->source, ($context["pagesOptions"] ?? null), "prev", [], "any", false, false, false, 6)]), "html", null, true);
                echo "\" aria-label=\"Previous\">
                        <span aria-hidden=\"true\">&laquo;</span>
                        <span class=\"sr-only\">Previous</span>
                    </a>
                </li>
            ";
            }
            // line 12
            echo "
            ";
            // line 13
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(range(1, twig_get_attribute($this->env, $this->source, ($context["pagesOptions"] ?? null), "total", [], "any", false, false, false, 13)));
            foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                // line 14
                echo "                <li class=\"page-item";
                if (($context["i"] == twig_get_attribute($this->env, $this->source, ($context["pagesOptions"] ?? null), "current", [], "any", false, false, false, 14))) {
                    echo " active";
                }
                echo "\">
                    <a class=\"page-link\" href=\"";
                // line 15
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, false, false, 15), "attributes", [], "any", false, false, false, 15), "get", [0 => "_route"], "method", false, false, false, 15), ["page" => $context["i"]]), "html", null, true);
                echo "\">
                        ";
                // line 16
                echo twig_escape_filter($this->env, $context["i"], "html", null, true);
                echo "
                    </a>
                </li>
            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 20
            echo "
            ";
            // line 21
            if ((twig_get_attribute($this->env, $this->source, ($context["pagesOptions"] ?? null), "current", [], "any", false, false, false, 21) != twig_get_attribute($this->env, $this->source, ($context["pagesOptions"] ?? null), "next", [], "any", false, false, false, 21))) {
                // line 22
                echo "                <li class=\"page-item\">
                    <a class=\"page-link\" href=\"";
                // line 23
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, false, false, 23), "attributes", [], "any", false, false, false, 23), "get", [0 => "_route"], "method", false, false, false, 23), ["page" => twig_get_attribute($this->env, $this->source, ($context["pagesOptions"] ?? null), "next", [], "any", false, false, false, 23)]), "html", null, true);
                echo "\" aria-label=\"Next\">
                        <span aria-hidden=\"true\">&raquo;</span>
                        <span class=\"sr-only\">Next</span>
                    </a>
                </li>
            ";
            }
            // line 29
            echo "        </ul>
    </nav>
";
        }
    }

    public function getTemplateName()
    {
        return "nav/pagination_simple.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  102 => 29,  93 => 23,  90 => 22,  88 => 21,  85 => 20,  75 => 16,  71 => 15,  64 => 14,  60 => 13,  57 => 12,  48 => 6,  45 => 5,  43 => 4,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "nav/pagination_simple.html.twig", "C:\\wamp64\\www\\videomaker\\templates\\nav\\pagination_simple.html.twig");
    }
}
