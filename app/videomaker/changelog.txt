
1.0.3
- Admin interface has been added with a list of projects and a password update.
- Language settings in services.yaml.

1.0.2
- Added settings in /config/services.yaml: app.display_language_switch, app.display_homepage_video, app.display_homepage_features.
- Added ability to integration via iframe (function "vssmExportUrl").

1.0.1
- Added options "app.watermark_image_path" and "app.watermark_position".

1.0.0
- First release.
