import {Component, OnInit, ViewChild} from '@angular/core';
import {Router} from '@angular/router';

import {MediaService} from '../services/media.service';

declare const appSettings: any;

@Component({
    selector: 'app-homepage',
    templateUrl: './homepage.component.html',
    styleUrls: ['./homepage.component.css']
})
export class HomepageComponent implements OnInit {

    @ViewChild('fileInput') fileInput;
    @ViewChild('sampleVideo') sampleVideo;
    loading = false;
    files: File[] = [];
    isSampleVideoVisible = true;
    displayLanguageSwitch: boolean;
    displayHomepageVideo: boolean;
    displayHomepageFeatures: boolean;

    constructor(
        private router: Router,
        private dataService: MediaService
    ) {
        this.displayLanguageSwitch = appSettings.displayLanguageSwitch;
        this.displayHomepageVideo = appSettings.displayHomepageVideo;
        this.displayHomepageFeatures = appSettings.displayHomepageFeatures;
    }

    ngOnInit() {

    }

    selectFiles(event?: MouseEvent): void {
        if (event) {
            event.preventDefault();
        }
        this.fileInput.nativeElement.click();
    }

    onFileSelect(): void {
        const files = this.fileInput.nativeElement.files;
        if (!files || files.length === 0) {
            return;
        }
        this.files.push(...files);
    }

    dragOverHandler(event: DragEvent): void {
        event.preventDefault();
    }

    dropHandler(event: DragEvent): void {
        event.preventDefault();
        if (event.dataTransfer.items) {
            for (let i = 0; i < event.dataTransfer.items.length; i++) {
                this.files.push(event.dataTransfer.items[i].getAsFile());
            }
        } else {
            for (let i = 0; i < event.dataTransfer.files.length; i++) {
                this.files.push(event.dataTransfer.files[i]);
            }
        }
    }

    uploadFiles(): void {
        this.loading = true;

        const data = {};
        this.files.forEach((file, index) => {
            data[`file${index}`] = file;
        });

        this.dataService.postFormData(this.dataService.getFormData(data))
            .subscribe((res) => {
                if (res['success']) {
                    this.clearFiles();
                    this.router.navigate(['/project', res['projectUniqueId']]);
                }
                this.loading = false;
            }, (err) => {
                this.loading = false;
            });
    }

    clearFiles(): void {
        this.files.splice(0, this.files.length);
    }

    sampleVideoHide(event?: MouseEvent): void {
        if (event) {
            event.preventDefault();
        }
        if (this.sampleVideo.nativeElement.tagName.toLowerCase() === 'video') {
            this.sampleVideo.nativeElement.pause();
        } else if (this.sampleVideo.nativeElement.tagName.toLowerCase() === 'div') {
            const iframe = this.sampleVideo.nativeElement.querySelector('iframe');
            iframe.contentWindow.postMessage('{"event":"command","func":"stopVideo","args":""}', '*');
        }
        this.isSampleVideoVisible = false;
    }

}
