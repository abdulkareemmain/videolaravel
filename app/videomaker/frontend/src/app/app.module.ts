import {BrowserModule} from '@angular/platform-browser';
import {HttpClientModule} from '@angular/common/http';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {registerLocaleData} from '@angular/common';

import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {DragulaModule} from 'ng2-dragula';
import {AngularCropperjsModule} from 'angular-cropperjs';
import {ModalModule} from 'ngx-bootstrap/modal';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {ProjectComponent} from './project/project.component';
import {HomepageComponent} from './homepage/homepage.component';
import {ModalPhotoEditComponent} from './project/modal-edit.component';

import localeEn from '@angular/common/locales/en';
import localeRu from '@angular/common/locales/ru';
import {TranslateCustomLoader} from './services/translateLoader';

registerLocaleData(localeEn, 'en-EN');
registerLocaleData(localeRu, 'ru-RU');

@NgModule({
    declarations: [
        AppComponent,
        ProjectComponent,
        HomepageComponent,
        ModalPhotoEditComponent
    ],
    imports: [
        BrowserModule,
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useClass: TranslateCustomLoader
            }
        }),
        HttpClientModule,
        FormsModule,
        AppRoutingModule,
        AngularCropperjsModule,
        DragulaModule.forRoot(),
        ModalModule.forRoot()
    ],
    entryComponents: [
        ModalPhotoEditComponent
    ],
    exports: [
        TranslateModule
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule {
}
