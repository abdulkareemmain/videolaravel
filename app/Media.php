<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class Media extends Model
{
    protected $table='media';
    protected $guarded=[];

    public function setUpdatedAt($value)
{
    // Do nothing.
}




const TYPE_AUDIO = 'audio';
const TYPE_IMAGE = 'image';
const TYPE_VIDEO = 'video';

/**
 * @ORM\Id()
 * @ORM\GeneratedValue()
 * @ORM\Column(type="integer")
 */
private $id;

/**
 * @ORM\Column(type="string", length=255)
 */
private $title;

/**
 * @ORM\Column(type="string", length=255)
 */
private $fileName;

/**
 * @ORM\Column(type="integer")
 */
private $size;

/**
 * @ORM\Column(type="datetime")
 */
private $createdAt;

/**
 * @var UploadedFile
 * @Assert\NotBlank(message="Please upload file.", groups={"image", "audio"})
 * @Assert\File(maxSize="20M", mimeTypes={"image/jpeg", "image/png", "image/gif"}, mimeTypesMessage="Invalid file type. You must upload an image file.", groups={"image"})
 * @Assert\File(maxSize="20M", mimeTypes={"audio/mp3", "application/octet-stream"}, mimeTypesMessage="Invalid file type. You must upload an audio file.", groups={"audio"})
 */
private $file;

/**
 * @ORM\Column(type="integer")
 */
private $projectId;

/**
 * @ORM\Column(type="string", length=128, nullable=true)
 */
private $type;

/**
 * @ORM\Column(type="integer", options={"default": 0})
 */
private $orderIndex = 0;

/**
 * @ORM\Column(type="string", length=255, nullable=true)
 */
private $textTitle;

/**
 * @ORM\Column(type="string", length=512, nullable=true)
 */
private $options;

/**
 * @ORM\Column(type="string", length=512, nullable=true)
 */
private $motionOptions;

/**
 * @ORM\Column(type="boolean", options={"default": true})
 */
private $isActive = true;

/**
 * @var string
 */
private $fileTargetDir;

/**
 * @ORM\PrePersist
 */
public function prePersist()
{
    $this->createdAt = new \DateTime();
    if ($this->file && $this->file instanceof UploadedFile && $this->getFileTargetDir()) {
        $ext = strtolower($this->file->getClientOriginalExtension());
        $this->setFileName($this->generateUniqueFileName() . '.' . $ext);
        $this->file->move($this->getFileTargetDir(), $this->getFileName());
    }
}

public function getId(): ?int
{
    return $this->id;
}

public function getTitle(): ?string
{
    return $this->title;
}

public function setTitle(string $title): self
{
    $this->title = $title;

    return $this;
}

public function getSize(): ?int
{
    return $this->size;
}

public function setSize(int $size): self
{
    $this->size = $size;

    return $this;
}

public function getType(): ?string
{
    return $this->type;
}

public function setType(?string $type): self
{
    $this->type = $type;

    return $this;
}

public function getProjectId(): ?int
{
    return $this->projectId;
}

public function setProjectId(int $projectId): self
{
    $this->projectId = $projectId;

    return $this;
}

public function getFile(): ?UploadedFile
{
    return $this->file;
}

public function setFile(UploadedFile $file): self
{
    $this->file = $file;
    return $this;
}

/**
 * @return string
 */
public static function generateUniqueFileName()
{
    return md5(uniqid());
}

public function getCreatedAt(): ?\DateTimeInterface
{
    return $this->createdAt;
}



public function getFileTargetDir(): ?string
{
    return $this->fileTargetDir;
}

public function setFileTargetDir(string $fileTargetDir): self
{
    $this->fileTargetDir = $fileTargetDir;
    return $this;
}

public function getFileName(): ?string
{
    return $this->fileName;
}

public function setFileName(string $fileName): self
{
    $this->fileName = $fileName;

    return $this;
}

public function getTextTitle(): ?string
{
    return $this->textTitle;
}

public function setTextTitle(?string $textTitle): self
{
    $this->textTitle = $textTitle;

    return $this;
}

public function getIsActive(): ?bool
{
    return $this->isActive;
}

public function setIsActive(bool $isActive): self
{
    $this->isActive = $isActive;

    return $this;
}

public function getOrderIndex(): ?int
{
    return $this->orderIndex;
}

public function setOrderIndex(int $orderIndex): self
{
    $this->orderIndex = $orderIndex;

    return $this;
}

public function getFilePath()
{
    $fileDirPath = $this->getFileTargetDir();
    return $fileDirPath . DIRECTORY_SEPARATOR . $this->getFileName();
}

public function getOptions(): array
{
    return $this->options ? json_decode($this->options, true) : [];
}

public function setOptions($options): self
{
    $this->options = json_encode($options, JSON_FORCE_OBJECT);
    return $this;
}

public function getOptionValue($optionName, $defaultValue = '')
{
    $options = $this->getOptions();
    return $options[$optionName] ?? $defaultValue;
}

public function setOptionValue($optionName, $value)
{
    $options = $this->getOptions();
    if (!is_array($options)) {
        $options = [];
    }
    $options[$optionName] = $value;
    $this->setOptions($options);
    return $this;
}

public function getMotionOptions(): array
{
    return $this->motionOptions ? json_decode($this->motionOptions, true) : [];
}

public function setMotionOptions($motionOptions): self
{
    $this->motionOptions = json_encode($motionOptions, JSON_FORCE_OBJECT);
    return $this;
}

public function toArray()
{
    return [
        'id' => $this->getId(),
        'title' => $this->getTitle(),
        'isActive' => $this->getIsActive(),
        'size' => $this->getSize(),
        'fileName' => $this->getFileName(),
        'textTitle' => $this->getTextTitle(),
        'orderIndex' => $this->getOrderIndex(),
        'options' => $this->getOptions(),
        'motionOptions' => $this->getMotionOptions()
    ];
}
}
